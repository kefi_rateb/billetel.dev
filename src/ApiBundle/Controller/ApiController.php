<?php

namespace ApiBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\FOSRestController;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Request\ParamFetcherInterface;

class ApiController extends FOSRestController {



    /**
     * pour l'annulation totale du panier
     *
     * @ApiDoc(
     *     section="Api Billetel",
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when the form has errors"
     *   }
     * )
     *
     * @param Request $request the request object
     *
     * @return String
     */

    public function cartsIdDeleteAction($cartId)
    {

    }

    /**
     * Récupérer le panier
     *
     * @ApiDoc(
     *     section="Api Billetel",
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when the form has errors"
     *   }
     * )
     *
     * @param Request $request the request object
     *
     * @return String
     */

    public function cartsIdGetAction($cartId)
    {
        return true;
    }


    /**
     * Récupérer les billets dématérialisés
     *
     * @ApiDoc(
     *     section="Api Billetel",
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when the form has errors"
     *   }
     * )
     *
     * @param Request $request the request object
     *
     * @return String
     */

    public function cartsClientsOrdersAction($id)
    {
        return $id;
    }



    /**
     * filtre liste des Coach.
     *
     * @ApiDoc(
     *     section="Api Billetel",
     *   resource = true,
     *      parameters={
     *         {"name"="search_filter", "dataType"="string", "required"=true, "description"="Critere de recherche"},
     *         {"name"="item_per_page", "dataType"="string", "required"=true, "description"="item par page"},
     *        {"name"="page_number", "dataType"="string", "required"=true, "description"="nombre du page"},
     *     },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when the form has errors"
     *   }
     * )
     *
     * @param Request $request the request object
     *
     * @return String
     */

    public function cartsAction(Request $request)
    {
        return $request;
    }

    /**
     * pour la création de panier
     *
     * @ApiDoc(
     *     section="Api Billetel",
     *   resource = true,
     *      parameters={
     *         {"name"="event_id", "dataType"="string", "required"=true, "description"="eventId"},
     *         {"name"="session_id", "dataType"="string", "required"=true, "description"="sessionId"},
     *        {"name"="customer_class_id", "dataType"="string", "required"=true, "description"="customerClassId"},
     *        {"name"="ticket_quantity", "dataType"="string", "required"=true, "description"="ticketQuantity"},
     *     },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when the form has errors"
     *   }
     * )
     *
     * @param Request $request the request object
     *
     * @return String
     */

    public function cartsIdCreatAction(Request $request)
    {

        $event_id = $request->get('event_id');
        $session_id = $request->get('session_id');
        $customer_class_id = $request->get('customer_class_id');
        $ticket_quantity = $request->get('ticket_quantity ');

        return true;
    }


    /**
     * pour l'ajout de lignes de commande à un panier existant
     *
     * @ApiDoc(
     *     section="Api Billetel",
     *   resource = true,
     *      parameters={
     *         {"name"="event_id", "dataType"="string", "required"=true, "description"="eventId"},
     *         {"name"="session_id", "dataType"="string", "required"=true, "description"="sessionId"},
     *        {"name"="customer_class_id", "dataType"="string", "required"=true, "description"="customerClassId"},
     *        {"name"="ticket_quantity", "dataType"="string", "required"=true, "description"="ticketQuantity"},
     *     },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when the form has errors"
     *   }
     * )
     *
     * @param Request $request the request object
     *
     * @return String
     */

    public function cartsIdCreatExist($cartId,Request $request)
    {

        $event_id = $request->get('event_id');
        $session_id = $request->get('session_id');
        $customer_class_id = $request->get('customer_class_id');
        $ticket_quantity = $request->get('ticket_quantity ');

        return true;
    }


    /**
     * choix ou non de l'assurance sur plusieurs lignes de commande
     *
     * @ApiDoc(
     *     section="Api Billetel",
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when the form has errors"
     *   }
     * )
     *
     * @param Request $request the request object
     *
     * @return String
     */

    public function cartsItemsInsurancesAction($cartId,Request $request)
    {
        return true;
    }


    /**
     * Renseigner le porteur du billet
     *
     * @ApiDoc(
     *     section="Api Billetel",
     *   resource = true,
     *      parameters={
     *         {"name"="item_id", "dataType"="string", "required"=true, ""},
     *         {"name"="ticket_id", "dataType"="string", "required"=true, "description"=""},
     *         {"name"="first_name", "dataType"="string", "required"=true, "description"=""},
     *         {"name"="last_name", "dataType"="string", "required"=true, "description"=""},
     *     },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when the form has errors"
     *   }
     * )
     *
     * @param Request $request the request object
     *
     * @return String
     */

    public function cartsHoldersAction($cartId,Request $request)
    {

        $item_id = $request->get('item_id');
        $ticket_id = $request->get('ticket_id');
        $first_name = $request->get('first_name');
        $last_name = $request->get('last_name ');
        return true;
    }

    /**
     * Récupérer les billets dématérialisés
     *
     * @ApiDoc(
     *     section="Api Billetel",
     *   resource = true,
     *      parameters={
     *         {"name"="search_filter", "dataType"="string", "required"=true, "description"="Critere de recherche"},
     *         {"name"="item_per_page", "dataType"="string", "required"=true, "description"="item par page"},
     *        {"name"="page_number", "dataType"="string", "required"=true, "description"="nombre du page"},
     *     },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when the form has errors"
     *   }
     * )
     *
     * @param Request $request the request object
     *
     * @return String
     */

    public function clientsOrdersItemsTicketsAction(Request $request)
    {
        return $request;
    }
    

 

    /**
     * Renseigner ou mettre à jour coordonnées du client
     *
     * @ApiDoc(
     *
     *     section="Api Billetel",
     *   resource = true,
     *      parameters={
     *        {"name"="status", "dataType"="integer", "required"=true, "description"="status"},
     *        {"name"="id", "dataType"="integer", "required"=true, "description"="id"},
     *     },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when the form has errors"
     *   }
     * )
     *
     * @param Request $request the request object
     *
     * @return String
     */
    public function EditclientIdAction(Request $request)
    {
        return true;
    }

    /**
     * Renseigner l'adresse d'envoi
     *
     * @ApiDoc(
     *
     *     section="Api Billetel",
     *   resource = true,
     *      parameters={
     *        {"name"="gender", "dataType"="integer", "required"=true, "description"="gender"},
     *     },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when the form has errors"
     *   }
     * )
     *
     * 
     * @param Request $request the request object
     *
     * @return String
     */

    public function deliveryAddressAction($cartId,Request $request)
    {
        $gender= $request->get('gender');
        return true;
    }
    
    /**
     * Renseigner le mode d'obtention
     *
     * @ApiDoc(
     *   section="Api Billetel",
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when the form has errors"
     *   }
     * )
     * 
     * @param Request $request the request object
     *
     * @return String
     */
    public function deliveryModeAction($cartId,$deliveryModeId)
    {
        return true;
    }
    
    /**
     * Renseigner ou mettre à jour coordonnées du client
     *
     * @ApiDoc(
     *   section="Api Billetel",
     *   resource = true,
     *   parameters={
     *      {"name"="coach_info", "dataType"="string", "required"=true, "description"="Id coach info"},
     *      {"name"="coach_event", "dataType"="integer", "required"=true, "description"="Id d'événement du coach"},
     *      {"name"="title", "dataType"="string", "required"=false, "description"="Titre d'événement"},
     *      {
                "name"="dates", 
                "dataType"="string", 
                "required"=false,
                "description"="Date(s) d'événement. Exemple: date_event[]=2016-01-01&time_event[]=00:00&date_event[]=2016-01-31&time_event[]=00:59 ..."
            },
     *      {"name"="description", "dataType"="string", "required"=false, "description"="Description d'évenément"},
     *      {"name"="photo", "dataType"="string", "required"=false, "description"="Photo d'évenément"},
     *      {"name"="status", "dataType"="string", "required"=false, "format"="[0, 1, ...]", "description"="Status d'événement : 0=En Attente d'activation, 1=Activer, 6=Supprimer, 7=Désactiver"},
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when the form has errors"
     *   }
     * )
     * 
     * @param Request $request the request object
     *
     * @return String
     */
    public function creatClientsAction(Request $request)
    {
        return $request;
    }
    
    /**
     * Détails d'un événement
     *
     * @ApiDoc(
     *   section="Api Billetel",
     *   resource = true,
     *   parameters={
     *      {"name"="coach_info", "dataType"="integer", "required"=true, "description"="Id coach info"},
     *      {"name"="coach_event", "dataType"="integer", "required"=true, "description"="Id d'événement du coach"}
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when the form has errors"
     *   }
     * )
     * 
     * @param Request $request the request object
     *
     * @return String
     */
    public function clientsOrdersAction(Request $request)
    {
        return $request;
    }



    /**
     * Valider le paiement sécurisé
     *
     * @ApiDoc(
     *   section="Api Billetel",
     *   resource = true,
     *   parameters={
     *      {"name"="coach_info", "dataType"="integer", "required"=true, "description"="Id coach info"},
     *      {"name"="coach_event", "dataType"="integer", "required"=true, "description"="Id d'événement du coach"}
     *   },
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when the form has errors"
     *   }
     * )
     *
     * @param Request $request the request object
     *
     * @return String
     */
    public function clientsOrders3DSecureAction(Request $request)
    {
        return $request;
    }
    
    /**
     * pour l'annulation d'une ligne de commande du panier
     *
     * @ApiDoc(
     *   section="Api Billetel",
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     400 = "Returned when the form has errors"
     *   }
     * )
     * 
     * @param Request $request the request object
     *
     * @return String
     */
    public function deleteCartsItemCardAction($cartId,$itemId)
    {
        return true;
    }




}
